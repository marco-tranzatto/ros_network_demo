# Ros Network Multimaster Demo

## Overview

This package contains launch files to show usage of [multimaster](http://wiki.ros.org/multimaster) package, for registering topics at a different ROS master and/or subscribing to topics of the same master.

The source code is released under a [BSD 3-Clause license](ros_package_template/LICENSE).

**Author(s): Marco Tranzatto, Kai Holtmann**

**Maintainer: Marco Tranzatto, marcot@ethz.ch **

**Affiliation: Robotics Systems Lab, ETH Zurich**

### Building from Source

#### Dependencies

- [multimaster](http://wiki.ros.org/multimaster)

		cd catkin_ws/src
		hg clone https://bitbucket.org/daenny/multimaster


#### Building

To build from source, clone the latest version from this repository into your catkin workspace and compile the package using

	cd catkin_ws/src
	git clone git@bitbucket.org:marco-tranzatto/ros_network_demo.git
	cd ../
	catkin build ros_network_multimaster_demo

## Usage

Example with 3 rosmasters running on the same machine (on different ports) (Exampled adapted from [using multiple cores](http://wiki.ros.org/ros-rt-wmp#Using_multiples_roscore)).

You will set up two "robots" (R1 and R2) and one PC that is supposed to use rviz to show data from the two robots. 
In real word scenario, with ROS masters running on different physical machine, a time synchronization may be required between the three agents (for example with chrony).

- R1 and R2 sends their **throttled** and **renamed** TF tree to PC. PC composes the two TF topics in standard `/tf` and uses RVIZ for visualization.
- R1 and R2 sends messages to PC: `/chatter_R1_to_PC` and `/chatter_R2_to_PC`. If PC modifies these topics, R1 and R2 do NOT get the any new message from PC.
- R1 exports one service to R2: `/service_R1_to_R2`.
- R1 and R2 synchronize on the same topic (each of them sees if the other publishes a new message): `/chatter_common_robots`.

In terminal #1

	ROS_MASTER_URI=http://localhost:11311
	roscore -p 11311 &
	roslaunch ros_network_multimaster_demo robot1.launch pc_rviz_master_hostname:="localhost" pc_rviz_master_port:="13311" robot2_master_hostname:="localhost" robot2_master_port:="12311" 

In terminal #2

	ROS_MASTER_URI=http://localhost:12311
	roscore -p 12311 &
	roslaunch ros_network_multimaster_demo robot2.launch pc_rviz_master_hostname:="localhost" pc_rviz_master_port:="13311"

In terminal #3

	ROS_MASTER_URI=http://localhost:13311
	roscore -p 13311 &
	roslaunch ros_network_multimaster_demo pc_rviz.launch

