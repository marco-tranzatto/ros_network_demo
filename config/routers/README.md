# Router configurations #

The binary files provided in this folder are used for the following routers and adhere to the below specified network structure:

Router company: Netgear

Router Model: Nighthawk R7000

Firmware: DD-WRT v3.0-r32170M kongac (06/11/17)

# Network structure #

| Router name   | Router username | Router password | IP          | Network SSID                 | Network Password | Subnet      | Gateway       |
|---------------|-----------------|-----------------|-------------|------------------------------|------------------|-------------|---------------|
| Parent_router | admin           | RosNetworkDemo  | 192.168.0.1 | parent_net_5 / parent_net_24 | RosNetworkDemo   | 192.168.0.0 | 255.255.255.0 |
| Child1_router | admin           | RosNetworkDemo  | 192.168.1.1 | child1_net_5 / child1_net_24 | RosNetworkDemo   | 192.168.1.0 | 255.255.255.0 |
| ...           |                 |                 |             |                              |                  |             |               |
| Child**n**_router | admin           | RosNetworkDemo  | 192.168.**n**.1 | child**n**_net_5 / child**n**_net_24 | RosNetworkDemo   | 192.168.**n**.0 | 255.255.255.0 |

Note, that the network SSID corresponds to the 2.4GHz and 5GHz network respectively.

# Load configuration files #

1. Power the router.

2. Connect to the router either via Ethernet cable or Wifi.

3. Open a browser window and access its configuration page by typing in the IP of the router.

4. Navigate to '*Backup*' under the '*Administration*' tab.

5. Select and load the desired configuration file.

# Add static routes #

After two (or more, i.e., parent_net and one or more child_net) subnets have been set up, add static routes to link the subnets.

The static routes are specified in the Parent router:

1. Connect routers by attaching Ethernet cables to the WAN port on the child routers and a LAN port on the parent router.

2. Open the parent router configuration site and navigate to '*Advanced Routing*' under '*Setup*'.

3. Add static routes for every child router connected to the parent router (i.e., for every child subnet that should be connected to the parent subnet).

For example if parent_net and child1_net should be connected, add a static route to the parent router with the following entries:

**Operating mode**: Gateway

**Route Name**: Child1

**Metric**: 2

**Destination LAN NET**: 192.168.1.0

**Subnet Mask**: 255.255.255.0

**Gateway**: 192.168.0.__*__

**Interface**: LAN & WLAN

where * corresponds to the WAN IP of the child router.

A more detailed explanation can be found [here](https://www.dd-wrt.com/wiki/index.php/Linking_Subnets_with_Static_Routes).
