# Ros Network Demo #

## Overview

Collections of nodes and launch files to test ROS networking examples with multiple masters.

- [Routers Configurations](https://bitbucket.org/marco-tranzatto/ros_network_demo/src//config/routers/?at=master)
- [Example of 'multimaster' usage](https://bitbucket.org/marco-tranzatto/ros_network_demo/src//ros_network_multimaster_demo/?at=master)